//
// Created by Администратор on 26.10.2017.
//

#ifndef SQUARE_POINT_H
#define SQUARE_POINT_H


#include <ostream>

class Point {
    double x, y;

public:
    Point operator+(Point &);

    Point &operator++();

    Point operator*(Point &);

    void printPoint(Point &);

    double getX() const;

    void setX(double x);

    double getY() const;

    void setY(double y);

    friend std::ostream &operator<<(std::ostream &, Point &);

    Point(double d = 0, double d1 = 0);

    Point(const Point &);

    Point &operator--();
};


#endif //SQUARE_POINT_H
