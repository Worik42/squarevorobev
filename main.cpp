#include <iostream>
#include "Point.h"
#include "Square.h"

using namespace std;


int main() {

    Square square;
    Point point(1, 1);
    square.setPoint(point);
    square.setSideSquare(2);

    Square square1;
    Point point1(2, 3);
    square1.setPoint(point1);
    square1.setSideSquare(1);

    cout << "create two square";
    cout << endl << "out square 1:" << endl;
    square.show();

    cout << endl << "out square 2:" << endl;
    square1.show();

    cout << endl << "square1 + square2 =:" << endl;
    Square square2;
    square2 = square + square1;
    square2.show();

    cout << endl << "square ++==" << endl;
    square2.operator++();
    square2.show();

    cout << endl << "square --==" << endl;
    square2.operator--();
    square2.show();

    cout << endl << "move to x=1 ,y=2:" << endl;
    Point p(1, 2);
    square2.move(p);
    square2.show();
    return 0;
}