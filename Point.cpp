//
// Created by Администратор on 26.10.2017.
//
#include <iostream>
#include "Point.h"

Point::Point(double d, double d1) {
    x = d;
    y = d1;
}


Point Point::operator+(Point &p) {
    return Point(x + p.x, y + p.y);
}

Point &Point::operator++() {
    y++;
    x++;
    return *this;
}

Point Point::operator*(Point &p) {
    return Point(x * p.x, y * p.y);
}

void Point::printPoint(Point &p) {
    std::cout << "X==" << p.x << "\n";
    std::cout << "Y==" << p.y << "\n";
}

double Point::getX() const {
    return x;
}

void Point::setX(double x) {
    Point::x = x;
}

double Point::getY() const {
    return y;
}

void Point::setY(double y) {
    Point::y = y;
}

Point &Point::operator--() {
    y--;
    x--;

    return *this;
}

std::ostream &operator<<(std::ostream &s, Point &p) {
    s << "X=" << p.x << "Y=" << p.y << "\n";
    return s;
}

Point::Point(const Point &) {

}