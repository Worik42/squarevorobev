//
// Created by Администратор on 26.10.2017.
//

#ifndef SQUARE_SQUARE_H
#define SQUARE_SQUARE_H


#include "Point.h"

class Square {
    double sideSquare;
    Point point;
public:

    Square(const Square &);

    double getSideSquare() const;

    void setSideSquare(double sideSquare);

    void setPoint(Point &);

    Point getPoint();

    Square operator++();

    Square operator--();

    void move(Point &);

    Square operator+(Square &);

    void show();

    virtual ~Square();

    Square();
};


#endif //SQUARE_SQUARE_H
