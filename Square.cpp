//
// Created by Администратор on 26.10.2017.
//

#include <iostream>
#include "Square.h"

using namespace std;

double Square::getSideSquare() const {
    return sideSquare;
}

void Square::setSideSquare(double sideSquare) {
    Square::sideSquare = sideSquare;
}


Square::~Square() {

}

Square::Square(const Square &) {

}

Square Square::operator++() {

    point.operator++();

    return *this;
}

Square Square::operator--() {
    point.operator--();
    return *this;
}

void Square::move(Point &point) {
    this->point.setX(this->point.getX() + point.getX());
    this->point.setY(this->point.getY() + point.getY());

}

Square Square::operator+(Square &square) {
    Square sq;
    double sideSquare2 = 0;
    double sideSquare1 = 0;
    if (point.getY() > square.point.getY()) {
        if ((point.getY() - sideSquare) > (square.point.getY() - square.sideSquare)) {
            sideSquare1 = point.getY() - (square.point.getY() - square.sideSquare);
            sq.point.setY(point.getY());
        }
    }
    if (point.getY() < square.point.getY()) {
        if ((point.getY() - sideSquare) < (square.point.getY() - square.sideSquare)) {
            sideSquare1 = square.point.getY() - (point.getY() - square.sideSquare);
            sq.point.setY(square.point.getY());
        }
    }

    if (point.getX() > square.point.getX()) {
        if ((point.getX() + sideSquare) > (square.point.getX() + square.sideSquare)) {
            sideSquare2 = point.getX() + sideSquare - square.point.getX();
            sq.point.setX(point.getX());
        }
    }
    if (point.getX() < square.point.getX()) {
        if ((point.getX() + sideSquare) < (square.point.getX() + square.sideSquare)) {
            sideSquare2 = square.point.getX() + sideSquare - point.getX();
            sq.point.setX(square.point.getX());
        }
    }
    if (sideSquare1 < sideSquare2) {
        if (point.getX() < square.point.getX())
            sq.point.setX(point.getX());
        else
            sq.point.setX(square.point.getX());
        if (point.getY() < square.point.getY())
            sq.point.setY(square.point.getY());
        else
            sq.point.setY(point.getY());

        sq.setSideSquare(sideSquare2);

    } else if (point.getX() < square.point.getX())
        sq.point.setX(point.getX());
    else
        sq.point.setX(square.point.getX());
    if (point.getY() < square.point.getY())
        sq.point.setY(square.point.getY());
    else
        sq.point.setY(point.getY());


    sq.setSideSquare(sideSquare1);

    return sq;
}


Square::Square() {

}

void Square::setPoint(Point &point) {
    this->point = point;
}

Point Square::getPoint() {
    return point;
}

void Square::show() {
    cout << "Side Square:" << sideSquare << endl;
    cout << "1 point:" << "(" << point.getX() << "," << point.getY() << ")" << endl;
    cout << "2 point:" << "(" << point.getX() + sideSquare << "," << point.getY() << ")" << endl;
    cout << "3 point:" << "(" << point.getX() + sideSquare << "," << point.getY() - sideSquare << ")" << endl;
    cout << "4 point:" << "(" << point.getX() << "," << point.getY() - sideSquare << ")" << endl;
}
